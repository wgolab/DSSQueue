#ifndef DSS_QUEUE_INL_H
#define DSS_QUEUE_INL_H

#ifndef DSS_QUEUE_H
#error "DSSQueue-inl.h" should be included only in "DSSQueue.h" file.
#endif

#include "DSSQueue.h"

void DSS::DSSQueue::enqueue_combined(int thread_id, 
    pmem::obj::persistent_ptr<QueueNode> node, uint64_t tag) {

  prep_enqueue(thread_id, node, tag);
  exec_enqueue(thread_id, node);
}

bool DSS::DSSQueue::dequeue_combined(int thread_id,
    QueueNode **node, uint64_t tag) {
  
  prep_dequeue(thread_id, node, tag);
  return exec_dequeue(thread_id, node);
}

std::string DSS::DSSQueue::toString() {
  return "DSS";
}

bool DSS::DSSQueue::isDSS() {
  return true;
}

#endif // DSS_QUEUE_INL_H