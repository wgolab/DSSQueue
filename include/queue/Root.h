#ifndef DSS_ROOT_H
#define DSS_ROOT_H

namespace DSS {

    template <class QueueType>
    class Root {
    public:
        alignas(ALIGNMENT) pmem::obj::persistent_ptr<QueueType> queue_;
        alignas(ALIGNMENT) pmem::obj::persistent_ptr<NodePool> nodePool_;

        Root() {}
    };
}

#endif // DSS_ROOT_H