#ifndef DSS_NODE_ENTRY_H
#define DSS_NODE_ENTRY_H

#include <atomic>
#include <cstdint>
#include <list>
#include <mutex>


/*

  Credits: This source code file is a simplified version of
  https://github.com/microsoft/pmwcas/blob/master/src/common/epoch.h
  written by Tianzheng Wang and Justin Levandoski (Copyright Microsoft, MIT License).

*/


namespace DSS {

	typedef uint64_t NodeEpoch;

	struct NodeEntry
	{
		NodeEntry()
				: protected_epoch(0),
				  thread_id(0)
		{
		}

		std::atomic<NodeEpoch> protected_epoch; // 8 bytes
		std::atomic<uint64_t> thread_id;    //  8 bytes

		/// Ensure that each Entry is CACHELINE_SIZE.
		char ___padding[48];

		void* operator new[](uint64_t count) {
			void *mem = nullptr;
			int n = posix_memalign(&mem, 64, count);
			return mem;
		}

		void operator delete[](void* p) {
			free(p);
		}

		void* operator new(uint64_t count);
		void operator delete(void* p);
	};
}

#endif //DSS_NODE_ENTRY_H
