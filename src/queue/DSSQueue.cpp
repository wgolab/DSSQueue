#include <set>
#include <libpmem.h>

#include "DSSQueue.h"
#include "Atomics.h"

using namespace DSS;

void DSSQueue::initialize() {

  phead_ = sentinel_;
  ptail_ = sentinel_;
  sentinel_->next_ = sentinel_;
  sentinel_->del_thread_index_ = -1;

  for (int i = 0; i < MAX_THREADS; i++){
    for (int j = 0; j < NUM_OF_ELEMENTS_IN_X; j++) {
      X[(i<<PADDING_BITS) + j] = 0;
    }
  }	  
}

void DSSQueue::recover()
{
  std::set<pmem::obj::persistent_ptr<QueueNode>> allNodes;

  pmem::obj::persistent_ptr<QueueNode> n;
  pmem::obj::persistent_ptr<QueueNode> h;
  pmem::obj::persistent_ptr<QueueNode> t;

  n = phead_;
  h = phead_;
  t = phead_;
  allNodes.insert(n);
  while (n != n->next_) {
    n = n->next_;
    allNodes.insert(n);
    if (n->del_thread_index_ == -1) {
    } else {
      h = n;
    }
    t = n;    
  }

  ptail_ = t;
  pmem_persist(&ptail_, sizeof(ptail_));

  phead_ = h;
  pmem_persist(&phead_, sizeof(phead_));

  for (int i = 0; i < MAX_THREADS; i++) {
    if (X[i<<PADDING_BITS] & ENQ_PREP_TAG) {
      // @TODO: (Mohammad): Make sure that adding UDSS tags does not affect this operation.
      uint64_t offset = X[i<<PADDING_BITS] & ~OP_TAG_MASK & ~UDSS_TAG_MASK;
      pmem::obj::persistent_ptr<QueueNode> n = sentinel_;

      n.raw_ptr()->off = offset;
      bool inset = allNodes.count(n) > 0;
      if (inset && (X[i<<PADDING_BITS] & ENQ_COMPL_TAG) == 0) {
        X[i<<PADDING_BITS] = X[i<<PADDING_BITS] | ENQ_COMPL_TAG;
        pmem_persist(&X[i<<PADDING_BITS], sizeof(X[i<<PADDING_BITS]));
      } else if (!inset && (X[i<<PADDING_BITS] & ENQ_COMPL_TAG) == 0
          && (n->del_thread_index_ != -1)) {

        X[i<<PADDING_BITS] = X[i<<PADDING_BITS] | ENQ_COMPL_TAG;
        pmem_persist(&X[i<<PADDING_BITS], sizeof(X[i<<PADDING_BITS]));
      }
    }
  }
}

void DSSQueue::prep_enqueue(int thread_id,
    pmem::obj::persistent_ptr<QueueNode> node, uint64_t tag)
{
  X[thread_id<<PADDING_BITS] =
      ((node.raw().off | ENQ_PREP_TAG) & (~UDSS_TAG_MASK)) |
      ((tag << NUM_OF_DSS_RELATED_BITS) & UDSS_TAG_MASK);

  pmem_persist(&X[thread_id<<PADDING_BITS],
      sizeof(X[thread_id<<PADDING_BITS]) * NUM_OF_ELEMENTS_IN_X);
}

void DSSQueue::exec_enqueue(int thread_id,
    pmem::obj::persistent_ptr<QueueNode> node)
{
  while (true) {
    pmem::obj::persistent_ptr<QueueNode> last = ptail_;
    pmem::obj::persistent_ptr<QueueNode> next = last->next_;

    if(last == ptail_) {
      // offset of new node to be enqueued
      uint64_t newOffset = node.raw().off;
      // offset of last node
      uint64_t oldOffset = last.raw().off;
      // offset of last node's next pointer
      uint64_t *nextOffsetPtr = &last->next_.raw_ptr()->off;

      // if next pointer loops back to the same node,
      // then it's truly the last node in the queue
      if (next == last) {
        if(CompareExchange64(nextOffsetPtr, oldOffset, newOffset) == oldOffset) {			
          pmem_persist(nextOffsetPtr, sizeof(newOffset));
          
          X[thread_id<<PADDING_BITS] = X[thread_id<<PADDING_BITS] | ENQ_COMPL_TAG;
          pmem_persist(&X[thread_id<<PADDING_BITS], sizeof(X[thread_id<<PADDING_BITS]));

          uint64_t *tailOffsetPtr = &ptail_.raw_ptr()->off;
          CompareExchange64(tailOffsetPtr, oldOffset, newOffset);

          return;
        }
      } else {
        pmem_persist(nextOffsetPtr, sizeof(newOffset));
        uint64_t *tailOffsetPtr = &ptail_.raw_ptr()->off;
        uint64_t nextOffset = next.raw().off;
        CompareExchange64(tailOffsetPtr, oldOffset, nextOffset);
      }
    }
  }
}

void DSSQueue::prep_dequeue(int thread_id, QueueNode **node, uint64_t tag)
{
  X[thread_id<<PADDING_BITS] = DEQ_PREP_TAG |
      ((tag << NUM_OF_DSS_RELATED_BITS) & UDSS_TAG_MASK);

  pmem_persist(&X[thread_id<<PADDING_BITS],
      sizeof(X[thread_id<<PADDING_BITS]) * NUM_OF_ELEMENTS_IN_X);
}

bool DSSQueue::exec_dequeue(int thread_id, QueueNode **node)
{
  while (true) {
    pmem::obj::persistent_ptr<QueueNode> first = phead_;
    pmem::obj::persistent_ptr<QueueNode> last = ptail_;
    pmem::obj::persistent_ptr<QueueNode> next = first->next_;

    if (first == phead_) {
      if (first == last) {
        // empty queue
        if (next == first) {
          // next still loops back to head node, end of queue
          X[thread_id<<PADDING_BITS] = X[thread_id<<PADDING_BITS] | EMPTY_TAG;
          pmem_persist(&X[thread_id<<PADDING_BITS], sizeof(uint64_t));
          return false;
        } else {
          uint64_t *nextOffsetPtr = &last->next_.raw_ptr()->off;
          pmem_persist(nextOffsetPtr, sizeof(uint64_t));
          
          uint64_t *tailOffsetPtr = &ptail_.raw_ptr()->off;
          uint64_t lastOffset = last.raw().off;	    
          uint64_t nextOffset = next.raw().off;
          CompareExchange64(tailOffsetPtr, lastOffset, nextOffset);
        }
      } else {
        // non-empty queue, save offset of head node
        uint64_t firstOffset = first.raw().off;
        uint64_t nextOffset = next.raw().off;
        uint64_t *headOffsetPtr = &phead_.raw_ptr()->off;
        int64_t *nextDeqThreadIdPtr = &next->del_thread_index_.get_rw();

        // BEGIN -- Nan Li's optimization
        if((*nextDeqThreadIdPtr) != -1 && first == phead_) {
          pmem_persist(nextDeqThreadIdPtr, sizeof(QueueNode::del_thread_index_));
          if(first == phead_) {
            CompareExchange64(headOffsetPtr, firstOffset, nextOffset);
          }
          continue;
        }
        // END -- Nan Li's optimization

        X[thread_id<<PADDING_BITS] = (X[thread_id<<PADDING_BITS] & UDSS_TAG_MASK)
            | firstOffset | DEQ_PREP_TAG;
        pmem_persist(&X[thread_id<<PADDING_BITS], sizeof(X[thread_id<<PADDING_BITS]));

        if (CompareExchange64(nextDeqThreadIdPtr, (int64_t)-1, (int64_t)thread_id) == -1) {
          pmem_persist(nextDeqThreadIdPtr, sizeof(QueueNode::del_thread_index_));

          CompareExchange64(headOffsetPtr, firstOffset, nextOffset);

          // return address of dequeued node, let caller extract
          // the dequeued value from the successor node
          *node  = first.get();
          return true;
        } else if (phead_ == first) {
          // help another dequeuing thread
          pmem_persist(nextDeqThreadIdPtr, sizeof(QueueNode::del_thread_index_));

          CompareExchange64(headOffsetPtr, firstOffset, nextOffset);
        }
      }
    }
  }    
}


ResolveStatus DSSQueue::resolve_enq (int thread_id) {
  pmem::obj::persistent_ptr<QueueNode> enode = sentinel_;

  enode.raw_ptr()->off = X[thread_id<<PADDING_BITS] & ~OP_TAG_MASK & ~UDSS_TAG_MASK;
  OperationEnum op = OperationEnum::ENQUEUE;
		
  if (X[thread_id<<PADDING_BITS] & ENQ_COMPL_TAG){
    return ResolveStatus(enode->data_, (X[thread_id<<PADDING_BITS] & UDSS_TAG_MASK) | op);
  
  } else {
    return ResolveStatus(StatusEnum::FAIL, (X[thread_id<<PADDING_BITS] & UDSS_TAG_MASK) | op);
  }
}

ResolveStatus DSSQueue::resolve_deq (int thread_id) {
  
  pmem::obj::persistent_ptr<QueueNode> dnode = sentinel_;
  dnode.raw_ptr()->off = X[thread_id<<PADDING_BITS] & ~OP_TAG_MASK & ~UDSS_TAG_MASK;
  
  OperationEnum op = OperationEnum::DEQUEUE;

  if (X[thread_id<<PADDING_BITS] == DEQ_PREP_TAG) {
    return ResolveStatus(StatusEnum::FAIL, (X[thread_id<<PADDING_BITS] & UDSS_TAG_MASK) | op);
  
  } else if (X[thread_id<<PADDING_BITS] == (DEQ_PREP_TAG | EMPTY_TAG)) {
    return ResolveStatus(StatusEnum::EMPTY, (X[thread_id<<PADDING_BITS] & UDSS_TAG_MASK) | op);
  // @TODO: (Mohammad): I am not quite sure whether (dnode != nullptr)
  // is a correct condition here. I just added it to solve a segmentation fault.
  
  } else if ((dnode != nullptr) && (dnode->next_->del_thread_index_ == thread_id)) {
    return ResolveStatus(dnode->next_->data_, (X[thread_id<<PADDING_BITS] & UDSS_TAG_MASK) | op);
  
  } else {
    return ResolveStatus(StatusEnum::FAIL, (X[thread_id<<PADDING_BITS] & UDSS_TAG_MASK) | op);
  }
}

ResolveStatus DSSQueue::resolve(int thread_id) {
  if (X[thread_id<<PADDING_BITS] & ENQ_PREP_TAG){
    return resolve_enq(thread_id);
  } else if (X[thread_id<<PADDING_BITS] & DEQ_PREP_TAG){
    return resolve_deq(thread_id);
  } else {
    return ResolveStatus(StatusEnum::FAIL,
        (X[thread_id<<PADDING_BITS] & UDSS_TAG_MASK) | OperationEnum::NO_OP);
  }
}

size_t DSSQueue::size()
{
  pmem::obj::persistent_ptr<QueueNode> n = phead_;
  size_t ret = 0;
  while (n->next_ != n) {
    ret++;
    n = n->next_;
  }
  return ret;
}