#ifndef DSS_NODE_EPOCH_H
#define DSS_NODE_EPOCH_H


/*

  Credits: This source code file is a simplified version of
  https://github.com/microsoft/pmwcas/blob/master/src/common/epoch.h
  written by Tianzheng Wang and Justin Levandoski (Copyright Microsoft, MIT License).

*/


#include <atomic>
#include <cstdint>
#include <list>
#include <mutex>

#include "NodeEntry.h"
#include "NodeEpochTable.h"

namespace DSS {

	class NodeEpochManager {
	public:
	  static constexpr uint64_t MAX_EPOCH = (uint64_t)-1;

	  NodeEpochManager();
	  NodeEpochManager(NodeEpoch val);

		void initialize();

		void protect(size_t thread_id) {
			epoch_table_->protect(current_epoch_.load(std::memory_order_relaxed), thread_id);
		}

		void unprotect(size_t thread_id) {
			epoch_table_->unprotect(thread_id);
		}

		NodeEpoch getCurrentEpoch() {
			return current_epoch_.load(std::memory_order_seq_cst);
		}

		bool isSafeToReclaim(NodeEpoch epoch) {
			return epoch <= safe_to_reclaim_epoch_.load(std::memory_order_relaxed);
		}

		void bumpCurrentEpoch();
		void computeNewSafeToReclaimEpoch(NodeEpoch currentEpoch);

		NodeEpoch getSafeToReclaimEpoch();

		std::atomic<NodeEpoch> current_epoch_;
		std::atomic<NodeEpoch> safe_to_reclaim_epoch_;
		NodeEpochTable* epoch_table_;
		unsigned int intialized;

	};
}

#endif //DSS_NODE_EPOCH_H
