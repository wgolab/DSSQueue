#ifndef DSS_PRODUCER_CONSUMER_TEST_H
#define DSS_PRODUCER_CONSUMER_TEST_H

#include "PerformanceTest.h"

namespace DSS {
    template<class QueueType> class PerformanceTest;

	template<class QueueType>
	class ProducerConsumerTest : public PerformanceTest<QueueType> {
		public:
			
			ProducerConsumerTest();
			
			void run(pmem::obj::persistent_ptr<QueueType> queue,
					pmem::obj::persistent_ptr<NodePool> pool);

			void produce(size_t thread_index,
					pmem::obj::persistent_ptr<QueueType> queue,
					pmem::obj::persistent_ptr<NodePool> pool);

			void consume(size_t thread_index,
					pmem::obj::persistent_ptr<QueueType> queue,
					pmem::obj::persistent_ptr<NodePool> pool);

			void startWorkerThreads(
					pmem::obj::persistent_ptr<QueueType> queue,
					pmem::obj::persistent_ptr<NodePool> pool,
					std::thread* threads[]);

			uint64_t getStartingPoint(size_t thread_index,
					pmem::obj::persistent_ptr<QueueType> queue);
	};
}

#endif // DSS_PRODUCER_CONSUMER_TEST_H