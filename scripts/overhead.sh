#!/bin/bash

# This script runs the program and creates a
# CSV file with the overhead of ADSS implementation.
# allocate_overhead_v_work.csv and throughput.csv

MMFILE=/mnt/pmem_numa3/${USER}_dssq
CORES="3,7,11,15,19,23,27,31,35,39,43,47,51,55,59,63,67,71,75,79"

cd ..
mkdir -p build
cd build
cmake ..
make -j20

rm $MMFILE


echo "Threads,Num_Operations,Enqueue_Work,Dequeue_Work,Total_Work,Total_Overhead,Overall" > allocate_overhead_v_work.csv
echo "Threads,Throughput" > throughput.csv
for i in {1,4,8,12,16,20}
do
    echo ""
    THREADS=$i

    THRU=`taskset -c $CORES ./DSSQueue -threads=$THREADS -mem_map_path=$MMFILE -test_type="overhead" -queue_type=UDSS | grep Throughput | cut -d' ' -f3`
    echo $THREADS, $THRU >> throughput.csv

    TIME=`taskset -c $CORES ./DSSQueue -threads=$THREADS -mem_map_path=$MMFILE -test_type="overhead" -queue_type=UDSS | grep Overhead | cut -d " " -f 2`
    echo $THREADS,$TIME >> allocate_overhead_v_work.csv

    echo "Thread = "$THREADS
    echo "Throughput = "$THRU
    echo "Num_Operations,Enqueue_Work,Dequeue_Work,Total_Work,Total_Overhead,Overall:"
    echo $TIME
done
