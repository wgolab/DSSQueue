#ifndef DSS_QUEUE_NODE_H
#define DSS_QUEUE_NODE_H

#include <libpmemobj++/persistent_ptr.hpp>

#include "NodeEpochManager.h"

#define ALIGNMENT 64

namespace DSS {

  class alignas(ALIGNMENT) QueueNode {
  public:
    pmem::obj::persistent_ptr<QueueNode> next_;
    pmem::obj::p<uint64_t> data_;
    alignas(ALIGNMENT) pmem::obj::p<uint64_t> epoch_deleted_;
    pmem::obj::p<int64_t> del_thread_index_;
    pmem::obj::p<bool> is_in_queue_ = false;

    inline QueueNode();

    inline void reset();
    inline void print();

    inline bool isFree();
    inline bool isUsed();
    inline bool isRemoved();
  };
}

#include "QueueNode-inl.h"

#endif // DSS_QUEUE_NODE_H