#ifndef DSS_QUEUE_H
#define DSS_QUEUE_H

#include <iostream>
#include <cstdint>
#include <cstddef>
#include <unordered_map>
#include <libpmemobj++/make_persistent.hpp>
#include <libpmemobj++/p.hpp>
#include <libpmemobj++/persistent_ptr.hpp>
#include <libpmemobj++/pool.hpp>
#include <libpmemobj++/transaction.hpp>

#include "NodeEpochManager.h"
#include "QueueNode.h"
#include "NodePoolPartition.h"
#include "NodePool.h"

#include "Root.h"
#include "ResolveStatus.h"

namespace DSS {
	// Lower 4 bits are used for DSS tags
	constexpr uint8_t OP_TAG_MASK = 0x0F;
	
	constexpr uint64_t ENQ_PREP_TAG = 1;
	constexpr uint64_t ENQ_COMPL_TAG = 2;
	constexpr uint64_t DEQ_PREP_TAG = 4;
	constexpr uint64_t EMPTY_TAG = 8;

	// Upper 18 bits are used for UDSS tags
	constexpr uint8_t NUM_OF_DSS_RELATED_BITS = 48;
	constexpr uint8_t NUM_OF_TAG_BITS = 16;
	constexpr uint64_t MAX_TAG_VALUE = 0x000000000000FFFF;
	constexpr uint64_t UDSS_TAG_MASK = 0xFFFF000000000000;
	constexpr uint64_t NO_UDSS_TAG = 0;

	constexpr uint8_t PADDING_BITS = 5;
	constexpr uint8_t NUM_OF_ELEMENTS_IN_X = 3;

	class DSSQueue {
	public:
		alignas(ALIGNMENT) pmem::obj::p<uint64_t> X[MAX_THREADS<<DSS::PADDING_BITS];
		alignas(ALIGNMENT) pmem::obj::persistent_ptr<DSS::QueueNode> phead_;
		alignas(ALIGNMENT) pmem::obj::persistent_ptr<DSS::QueueNode> ptail_;
		alignas(ALIGNMENT) pmem::obj::persistent_ptr<DSS::QueueNode> sentinel_;

		void initialize();

		inline void enqueue_combined(int thread_id, pmem::obj::persistent_ptr<
				DSS::QueueNode> node, uint64_t tag);
	  void prep_enqueue(int thread_id, pmem::obj::persistent_ptr<
				DSS::QueueNode> node, uint64_t tag);
	  void exec_enqueue(int thread_id, pmem::obj::persistent_ptr<
				DSS::QueueNode> node);

		inline bool dequeue_combined(int thread_id, DSS::QueueNode **node, uint64_t tag);
	  void prep_dequeue(int thread_id, DSS::QueueNode **node, uint64_t tag);
	  bool exec_dequeue(int thread_id, DSS::QueueNode **node);

	  ResolveStatus resolve_enq (int thread_id);
	  ResolveStatus resolve_deq (int thread_id);
	  ResolveStatus resolve(int thread_id);

	  void recover();
	  size_t size();
		inline static std::string toString();
		inline bool isDSS();
	};
}

#include "DSSQueue-inl.h"

#endif //DSS_QUEUE_H
