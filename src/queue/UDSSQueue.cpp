#include "UDSSQueue.h"
#include <libpmem.h>

using namespace DSS;

ResolveStatus UDSSQueue::resolve(int thread_id) {
  ResolveStatus DSS_resolution = DSSQueue::resolve(thread_id);

  if ((DSS_resolution.operation_tag & OP_TAG_MASK) != OperationEnum::NO_OP
      && DSS_resolution.status != StatusEnum::FAIL) {

    X[(thread_id<<PADDING_BITS) + ResolveStatusPositions::STATUS] = DSS_resolution.status;
    X[(thread_id<<PADDING_BITS) + ResolveStatusPositions::OPERATION_TAG] = DSS_resolution.operation_tag;
    // important not to re-read lastOp, which is flushed from the cache
    return DSS_resolution;
  } else {
    return ResolveStatus(
        X[(thread_id<<PADDING_BITS) + ResolveStatusPositions::STATUS],
        X[(thread_id<<PADDING_BITS) + ResolveStatusPositions::OPERATION_TAG]);
  }
}