#include "QueueBenchmark.h"

using namespace DSS;

template<class QueueType, class TestType>
int start_benchmark() {

	QueueBenchmark<QueueType, TestType> queueBenchmark;

	if (queueBenchmark.initialize() != 0) {
		return -1;
	}
	
	queueBenchmark.run();
	return queueBenchmark.teardown();
}

template<class QueueType>
int run_benchmark() {
	if (FLAGS_test_type == RESOLUTION_TEST)
		return start_benchmark<QueueType, ResolutionTest<QueueType>>();
	else if (FLAGS_test_type == OVERHEAD_TEST)
		return start_benchmark<QueueType, OverheadTest<QueueType>>();	
	else if (FLAGS_test_type == PRODUCER_CONSUMER_TEST)
		return start_benchmark<QueueType, ProducerConsumerTest<QueueType>>();	
	else
		return start_benchmark<QueueType, PerformanceTest<QueueType>>();
}

int main(int argc, char* argv[]) {
	gflags::ParseCommandLineFlags(&argc, &argv, true);

	if (FLAGS_queue_type == UDSSQueue::toString()) {
		run_benchmark<UDSSQueue>();
	} else if (FLAGS_queue_type == DSSQueue::toString()) {
		run_benchmark<DSSQueue>();
	} else {
		run_benchmark<ADSSQueue>();
	}
}