#ifndef DSS_QUEUE_TEST_BASE_INL_H
#define DSS_QUEUE_TEST_BASE_INL_H

#ifndef DSS_QUEUE_TEST_BASE_H
#error "QueueTestBase-inl.h" should be included only in "QueueTestBase.h" file.
#endif

#include "QueueTestBase.h"
#include <random>
#include "UDSSQueue.h"

DECLARE_uint64(queue_size);

template<class QueueType>
DSS::QueueTestBase<QueueType>::QueueTestBase() {
  this->barrier = 0;
  this->finish_flag = 0;
  this->finish_barrier = 0;
  
  this->recoverNum_ = 0;
}

template<class QueueType>
void DSS::QueueTestBase<QueueType>::setup(
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<NodePool> pool) {

	epochMgr.initialize();

	// ensure the queue has the correct initial size
	epochMgr.protect(MAX_THREADS - 1);
	size_t s = queue->size();

	if (s < FLAGS_queue_size) {
	  for (int i = 0; i < FLAGS_queue_size - s; i++) {
	    enqueue(MAX_THREADS - 1, queue, pool, i, NO_UDSS_TAG);
	  }
	} else if (s > FLAGS_queue_size) {
	  uint64_t data;
	  for (int i = 0; i < s - FLAGS_queue_size; i++) {
	    dequeue(MAX_THREADS - 1, queue, pool, &data, NO_UDSS_TAG);
	  }
	}
	epochMgr.unprotect(MAX_THREADS - 1);
}      

template<class QueueType>
void DSS::QueueTestBase<QueueType>::teardown(std::thread* threads[]) {
	for (unsigned int i = 0; i < FLAGS_threads; i++) {
		threads[i]->join();
		delete threads[i];
	}
}

template<class QueueType>
void DSS::QueueTestBase<QueueType>::waitForExperiment() {

	while (this->barrier > 0);
	
	for (int i = 0; i < FLAGS_num_seconds; i++) {
		sleep(1);
		std::cout << "." << std::flush;
	}
	std::cout << std::endl;

	this->finish_flag = 1;

	while (this->finish_barrier > 0);
}

template<class QueueType>
pmem::obj::persistent_ptr<DSS::QueueNode>
		DSS::QueueTestBase<QueueType>::getNewNode(
		size_t thread_index,
        pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<DSS::NodePool> pool,
        uint64_t data) {

	pmem::obj::persistent_ptr<QueueNode> newNode;

	while (true) {
		newNode = pool->allocate(thread_index, &epochMgr, queue->phead_);

		if (newNode != nullptr) {
			break;
		}

		if (finish_flag) {
			throw std::bad_alloc();
		}
	}

  newNode->del_thread_index_ = -1;
  newNode->next_ = newNode;
  newNode->data_ = data;

  pmem_persist(&*newNode, sizeof(QueueNode));

	return newNode;
}

template<class QueueType>
void DSS::QueueTestBase<QueueType>::enqueue(size_t thread_index,
        pmem::obj::persistent_ptr<QueueType> queue,
        pmem::obj::persistent_ptr<NodePool> pool,
        uint64_t data, uint64_t tag) {

	pmem::obj::persistent_ptr<QueueNode> newNode = 
			getNewNode(thread_index, queue, pool, data);

	queue->enqueue_combined(thread_index, newNode, tag);
}

template<class QueueType>
bool DSS::QueueTestBase<QueueType>::dequeue(size_t thread_index,
        pmem::obj::persistent_ptr<QueueType> queue,
        pmem::obj::persistent_ptr<NodePool> pool,
        uint64_t *data, uint64_t tag) {
  QueueNode *deqNode;

	bool ret = queue->dequeue_combined(thread_index, &deqNode, tag);

  if (ret) {
    // successfully dequeued a node
    *data = deqNode->next_->data_;
    pool->free(thread_index, &epochMgr, deqNode);
  } else {
    // queue was empty
    *data = 0;
  }

  return ret;
}

#endif // DSS_QUEUE_TEST_BASE_INL_H