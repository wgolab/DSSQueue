#include "QueueBenchmark.h"

using namespace DSS;

// template types that we expect to see in our benchmark
template class QueueBenchmark<ADSSQueue, PerformanceTest<ADSSQueue>>;
template class QueueBenchmark<UDSSQueue, PerformanceTest<UDSSQueue>>;
template class QueueBenchmark<DSSQueue, PerformanceTest<DSSQueue>>;

template class QueueBenchmark<ADSSQueue, ResolutionTest<ADSSQueue>>;
template class QueueBenchmark<UDSSQueue, ResolutionTest<UDSSQueue>>;
template class QueueBenchmark<DSSQueue, ResolutionTest<DSSQueue>>;

template class QueueBenchmark<ADSSQueue, DSS::OverheadTest<ADSSQueue>>;
template class QueueBenchmark<UDSSQueue, DSS::OverheadTest<UDSSQueue>>;
template class QueueBenchmark<DSSQueue, DSS::OverheadTest<DSSQueue>>;

template class QueueBenchmark<ADSSQueue, DSS::ProducerConsumerTest<ADSSQueue>>;
template class QueueBenchmark<UDSSQueue, DSS::ProducerConsumerTest<UDSSQueue>>;
template class QueueBenchmark<DSSQueue, DSS::ProducerConsumerTest<DSSQueue>>;

// use gflags for variables can be modified using cmd
DEFINE_uint64(queue_size, 4, "");
DEFINE_uint64(threads, 2, "number of threads to use for multi-threaded tests");
DEFINE_uint64(num_seconds, 3, "experiment duration in seconds");
DEFINE_uint64(num_producers, 1, "Number of producers");
DEFINE_uint64(num_consumers, 1, "Number of consumers");
DEFINE_string(mem_map_path, "./mmfile.dat", "memory map file path");
DEFINE_string(test_type, "performance", "Test type [performance|resolution|overhead|producer-consumer]");
DEFINE_string(queue_type, "ADSS", "Queue type [DSS|ADSS|UDSS]");

template <class QueueType, class TestType>
void QueueBenchmark<QueueType, TestType>::createQueue() {
  // root object already exists, obtained earlier via pool.root
  root->queue_ = pmem::obj::make_persistent<QueueType>();
  root->queue_->sentinel_ = pmem::obj::make_persistent<QueueNode>();
  root->queue_->initialize();
	
  root->nodePool_ = pmem::obj::make_persistent<NodePool>();
  
  std::cout << "Progress: " << std::flush;

  for (int i = 0; i < NUM_PARTITIONS; i++) {
    root->nodePool_->partitions[i] = pmem::obj::make_persistent<NodePoolPartition>();

    for (int j = 0; j < NUM_NODES_PER_PARTITION; j++) {
      root->nodePool_->partitions[i]->nodes[j] = pmem::obj::make_persistent<QueueNode>();
    }

    std::cout << 100.0*(i+1)/NUM_PARTITIONS << "% " << std::flush;
  }
  std::cout << std::endl;
}

template <class QueueType, class TestType>
int QueueBenchmark<QueueType, TestType>::initialize() {

  try {
    if (access(FLAGS_mem_map_path.c_str(), F_OK)) {

      std::cout << "File does not exist, trying to create..." << std::endl;
			
      pool = pmem::obj::pool<Root<QueueType>>::create(FLAGS_mem_map_path.c_str(),
					LAYOUT, POOL_SIZE, S_IWUSR | S_IRUSR);
      root = pool.root();

      pmem::obj::transaction::run(pool, [&] {
					  createQueue();
					});
    } else {
      std::cout << "File exists, trying to open..." << std::endl;
      pool = pmem::obj::pool<Root<QueueType>>::open(FLAGS_mem_map_path, LAYOUT);
      root = pool.root();

      pmem::obj::transaction::run(pool, [&] {
        root->queue_->recover();
        root->nodePool_->recover();
      });
      // WG: there could be memory leaks, need to fix them
    }

    std::cout << "Successfully retrieved root object..." << std::endl;
  } catch (const pmem::pool_error &e) {
    std::cerr << "Exception: " << e.what() << std::endl;
    return 1;
  } catch (const pmem::transaction_error &e) {
    std::cerr << "Exception: " << e.what() << std::endl;
    return 1;
  }
  return 0;
}

template <class QueueType, class TestType>
void QueueBenchmark<QueueType, TestType>::run() {

  try {    
    TestType test;
    test.run(root->queue_, root->nodePool_);
				
  } catch (const std::logic_error &e) {
    std::cerr << "Exception: " << e.what() << std::endl;
  }
}

template <class QueueType, class TestType>
int QueueBenchmark<QueueType, TestType>::teardown() {
  try {
    pool.close();
  } catch (const std::logic_error &e) {
    std::cerr << "Exception: " << e.what() << std::endl;
    return 1;
  }
  return 0;
}
