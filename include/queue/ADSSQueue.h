#ifndef ADSS_QUEUE_H
#define ADSS_QUEUE_H

#include "DSSQueue.h"
#include "Root.h"

namespace DSS {
	
	class ADSSQueue : public DSSQueue {
	public:
		// The following two methods are added to keep the same interface
		// with DSSQueue to be called from the benchmark
		inline void enqueue_combined(int thread_id, pmem::obj::
	  		persistent_ptr<QueueNode> node, uint64_t tag);
		inline bool dequeue_combined(int thread_id, QueueNode **node, uint64_t tag);

		inline static std::string toString();
		inline bool isDSS();
	};
}

#include "ADSSQueue-inl.h"

#endif // ADSS_QUEUE_H