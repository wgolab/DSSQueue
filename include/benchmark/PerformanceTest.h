#ifndef DSS_QUEUE_TEST_H
#define DSS_QUEUE_TEST_H

#include "QueueTestBase.h"

namespace DSS {
    template<class QueueType> class QueueTestBase;

template<class QueueType>
	class PerformanceTest : public QueueTestBase<QueueType> {
		protected:
			std::atomic<uint64_t> total_success_;
			std::atomic<uint64_t> enqNum_;
			std::atomic<uint64_t> deqNum_;
			
			int threadIdOpNumMap_[MAX_THREADS];

			alignas(ALIGNMENT) pmem::obj::p<uint64_t> A[MAX_THREADS<<DSS::PADDING_BITS];
			alignas(ALIGNMENT) pmem::obj::p<uint64_t> B[MAX_THREADS<<DSS::PADDING_BITS];
					
		public:
			PerformanceTest();

			void run(pmem::obj::persistent_ptr<QueueType> queue,
					pmem::obj::persistent_ptr<NodePool> pool);

			void main(size_t thread_index,
					pmem::obj::persistent_ptr<QueueType> queue,
					pmem::obj::persistent_ptr<NodePool> pool);

			void startWorkerThreads(
					pmem::obj::persistent_ptr<QueueType> queue,
					pmem::obj::persistent_ptr<NodePool> pool,
					std::thread* threads[]);

			void reportExperimentResult(pmem::obj::persistent_ptr<QueueType> queue);

			void enqueue(size_t thread_index,
					pmem::obj::persistent_ptr<QueueType> queue,
					pmem::obj::persistent_ptr<NodePool> pool,
					uint64_t n_enq);
	};
}

#endif // DSS_QUEUE_TEST_H