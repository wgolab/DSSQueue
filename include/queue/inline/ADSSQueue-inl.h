#ifndef ADSS_QUEUE_INL_H
#define ADSS_QUEUE_INL_H

#ifndef ADSS_QUEUE_H
#error "ADSSQueue-inl.h" should be included only in "ADSSQueue.h" file.
#endif

#include "ADSSQueue.h"
#include <libpmem.h>

void DSS::ADSSQueue::enqueue_combined(int thread_id, pmem::obj::
    persistent_ptr<DSS::QueueNode> node, uint64_t tag) {
  
  DSS::DSSQueue::prep_enqueue(thread_id, node, tag);
  DSS::DSSQueue::exec_enqueue(thread_id, node);
}

bool DSS::ADSSQueue::dequeue_combined(int thread_id,
    DSS::QueueNode **node, uint64_t tag) {
  
  DSS::DSSQueue::prep_dequeue(thread_id, node, tag);
  return DSS::DSSQueue::exec_dequeue(thread_id, node);
}

std::string DSS::ADSSQueue::toString() {
  return "ADSS";
}

bool DSS::ADSSQueue::isDSS() {
  return false;
}

#endif // ADSS_QUEUE_INL_H