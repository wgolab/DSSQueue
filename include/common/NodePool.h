#ifndef DSS_NODE_POOL_H 
#define DSS_NODE_POOL_H

#include <iostream>
#include <cstdint>
#include <cstddef>
#include <unordered_map>
#include <libpmemobj++/make_persistent.hpp>
#include <libpmemobj++/p.hpp>
#include <libpmemobj++/persistent_ptr.hpp>
#include <libpmemobj++/pool.hpp>
#include <libpmemobj++/transaction.hpp>

#include "NodeEpochManager.h"
#include "QueueNode.h"
#include "NodePoolPartition.h"

namespace DSS {  
  class NodePool {
  public:
    pmem::obj::persistent_ptr<NodePoolPartition> partitions[NUM_PARTITIONS];
		
    NodePool() {}	
		
    pmem::obj::persistent_ptr<QueueNode> allocate(uint64_t thread_index,
        NodeEpochManager *epochMgr,
        pmem::obj::persistent_ptr<DSS::QueueNode> phead_) {
          
      return partitions[thread_index]->allocate(thread_index, epochMgr, phead_);
    }

    void free(uint64_t thread_index, NodeEpochManager *epochMgr, QueueNode *node) {
      node->epoch_deleted_ = epochMgr->getCurrentEpoch();
    }

    void recover();
  };
}

#endif // DSS_NODE_POOL_H 
