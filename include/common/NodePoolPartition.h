#ifndef DSS_NODE_POOL_PARTITION_H 
#define DSS_NODE_POOL_PARTITION_H

#include <iostream>
#include <cstdint>
#include <cstddef>
#include <unordered_map>
#include <libpmem.h>
#include <libpmemobj++/make_persistent.hpp>
#include <libpmemobj++/p.hpp>
#include <libpmemobj++/persistent_ptr.hpp>
#include <libpmemobj++/pool.hpp>
#include <libpmemobj++/transaction.hpp>

#include "NodeEpochManager.h"
#include "QueueNode.h"

#define MAX_THREADS 20
#define NUM_PARTITIONS MAX_THREADS
#define NUM_NODES_PER_PARTITION (MAX_THREADS*100)

namespace DSS {
  class NodePoolPartition {

  public:
    pmem::obj::persistent_ptr<DSS::QueueNode> nodes[NUM_NODES_PER_PARTITION];
  	int last_allocated;
		
    NodePoolPartition();
	pmem::obj::persistent_ptr<DSS::QueueNode> allocate(uint64_t thread_index,
			NodeEpochManager *epochMgr,
			pmem::obj::persistent_ptr<DSS::QueueNode> phead_);

    void reclaim(uint64_t thread_index, NodeEpochManager *epochMgr);
    void recover();
  };
}

#endif // DSS_NODE_POOL_PARTITION_H
