#include "NodePoolPartition.h"

using namespace DSS;

NodePoolPartition::NodePoolPartition()
: last_allocated(-1)
{
}

pmem::obj::persistent_ptr<DSS::QueueNode> NodePoolPartition::allocate(
        uint64_t thread_index, NodeEpochManager *epochMgr,
        pmem::obj::persistent_ptr<DSS::QueueNode> phead_) {

    for (int j = 0; j < NUM_NODES_PER_PARTITION; j++) {
        int index = (j + last_allocated + 1) % NUM_NODES_PER_PARTITION;
        if (nodes[index]->isFree()) {
            nodes[index]->epoch_deleted_ = NodeEpochManager::MAX_EPOCH;
            last_allocated = index;
            return nodes[index];
        }
    }

    reclaim(thread_index, epochMgr);
    pmem_persist(&*phead_, sizeof(QueueNode));
    
    return nullptr;
}

void NodePoolPartition::reclaim(uint64_t thread_index,
        NodeEpochManager *epochMgr) {

    NodeEpoch safeEpoch = epochMgr->getSafeToReclaimEpoch();
    for (int j = 0; j < NUM_NODES_PER_PARTITION; j++) {
        if (nodes[j].get()->isRemoved() && nodes[j]->epoch_deleted_ <= safeEpoch) {
            nodes[j]->epoch_deleted_ = 0;
        }
    }
}

void NodePoolPartition::recover() {

    for (int j = 0; j < NUM_NODES_PER_PARTITION; j++) {
            if (nodes[j].get()->isRemoved()) {
                nodes[j]->epoch_deleted_ = 0;
            }
            if (nodes[j].get()->isUsed() && !nodes[j]->is_in_queue_){
                nodes[j]->epoch_deleted_ = 0;
            }
    }
    // WG: any node that was allocated but not added to the queue should also be reclaimed to prevent memory leaks
}