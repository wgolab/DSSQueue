#ifndef DSS_RESOLUTION_TEST_H
#define DSS_RESOLUTION_TEST_H

#include <vector>

#include "QueueTestBase.h"

namespace DSS {

	template<class QueueType> class QueueTestBase;

	template<class QueueType>
    class ResolutionTest : public QueueTestBase<QueueType> {
	private:
		typedef void(ResolutionTest::*UnitTest)(size_t,
				pmem::obj::persistent_ptr<QueueType>,
				pmem::obj::persistent_ptr<DSS::NodePool>);

		char result[MAX_THREADS][255];
		std::vector<UnitTest> unit_tests;
		std::vector<std::vector<std::string>> failed_tests;
		int num_of_failed_tests;
	public:
		// Unit tests.
		// Add the unit test's name to initializeUnitTests() method
		void ItemEnqueuedCompEnqTagExpected(size_t thread_index,
				pmem::obj::persistent_ptr<QueueType> queue,
				pmem::obj::persistent_ptr<DSS::NodePool> pool);

		void ItemEnqueuedThenDequeuedCompDeqTagExpected(size_t thread_index,
				pmem::obj::persistent_ptr<QueueType> queue,
				pmem::obj::persistent_ptr<DSS::NodePool> pool);

		void prepEnqueueIsInvokedPrepEnqTagExpected(size_t thread_index,
				pmem::obj::persistent_ptr<QueueType> queue,
				pmem::obj::persistent_ptr<DSS::NodePool> pool);

		void ItemEnqueuedThenPrepDequeueIsInvokedPrepDeqTagExpected(size_t thread_index,
				pmem::obj::persistent_ptr<QueueType> queue,
				pmem::obj::persistent_ptr<DSS::NodePool> pool);

		// Setup functions.
		void setupUnitTest(size_t thread_index);
		void teardownUnitTest(size_t thread_index);

		void startWorkerThreads(
				pmem::obj::persistent_ptr<QueueType> queue,
				pmem::obj::persistent_ptr<DSS::NodePool> pool,
				std::thread* threads[], UnitTest unit_test);

		void run(pmem::obj::persistent_ptr<QueueType> queue,
			pmem::obj::persistent_ptr<DSS::NodePool> pool);

		void initializeUnitTests();
		void run_tests(const std::vector<UnitTest>& tests,
				pmem::obj::persistent_ptr<QueueType> queue,
				pmem::obj::persistent_ptr<DSS::NodePool> pool);

		void printFailure(size_t thread_index, std::string functionName);
		void printSuccess(size_t thread_index, std::string functionName);
		void printTestReport();
	};

	enum ColorIndex {
		BLACK,
		RED,
		GREEN,
		BLUE,
		RESET
	};

	constexpr char ColorCodes[5][250] = {
		"\033[1m\033[30m",              // Bold BLACK
		"\033[1m\033[31m",              // Bold RED
		"\033[1m\033[32m",              // Bold GREEN
		"\033[1m\033[34m",              // Bold BLUE
		"\x1B[0m"						// Reset
	};
}

#endif // DSS_RESOLUTION_TEST_H