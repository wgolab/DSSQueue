#!/bin/bash

# This script runs the program with the performance test suite.
# It will create a CSV file called perf-DSS-ADSS-UDSS.csv
# with the throughput of DSS, ADSS, and UDSS queue implementations.

MMFILE=/mnt/pmem_numa3/${USER}_dssq
CORES="3,7,11,15,19,23,27,31,35,39,43,47,51,55,59,63,67,71,75,79"
NUM_OF_EXPR=1

exectue() {
    THRU=`taskset -c $CORES ./DSSQueue -threads=$THREADS\
            -mem_map_path=$MMFILE -queue_type=$queue_type\
            -test_type="performance" \
            | grep Throughput | cut -d' ' -f3`

    echo $THREADS, $THRU
    avg=$(($avg + $THRU))
}

run_performance_test() {
    rm perf-$queue_type.csv

    echo "num_threads,$queue_type" >> perf-$queue_type.csv
    
    rm $MMFILE
    for i in {1,4,8,12,16,20}
    do
        avg=0
        for (( j=1; j<=$NUM_OF_EXPR; j++ ))
        do
            THREADS=$i
            exectue
        done
        echo $THREADS,$((avg / NUM_OF_EXPR)) >> perf-$queue_type.csv
    done
}

cd ..
mkdir -p build
cd build
cmake ..
make -j20

for queue_type in {"ADSS","UDSS"}; do
    echo "--------------------------"
    echo $queue_type

    run_performance_test
done

awk -F, '{print $2}' OFS=, perf-UDSS.csv | paste -d, perf-ADSS.csv - > perf-ADSS-UDSS.csv
awk -F, '{print $2}' OFS=, perf-DSS.csv | paste -d, perf-ADSS-UDSS.csv - > perf-DSS-ADSS-UDSS.csv