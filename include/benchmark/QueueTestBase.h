#ifndef DSS_QUEUE_TEST_BASE_H
#define DSS_QUEUE_TEST_BASE_H

#include "QueueBenchmark.h"

DECLARE_uint64(threads);
DECLARE_uint64(num_seconds);

namespace DSS {

	template<class QueueType>
    class QueueTestBase {
	protected:
		NodeEpochManager epochMgr;

		std::atomic<int> barrier;
		std::atomic<int> finish_flag;
		std::atomic<int> finish_barrier;
		std::atomic<uint64_t> recoverNum_;
		
	public:
		inline QueueTestBase();

		inline void setup(pmem::obj::persistent_ptr<QueueType> queue,
				pmem::obj::persistent_ptr<NodePool> pool);

		inline pmem::obj::persistent_ptr<QueueNode> getNewNode(size_t thread_index,
        		pmem::obj::persistent_ptr<QueueType> queue,
				pmem::obj::persistent_ptr<NodePool> pool,
				uint64_t data);

		inline void teardown(std::thread* threads[]);

		inline void enqueue(size_t thread_index,
				pmem::obj::persistent_ptr<QueueType> queue,
				pmem::obj::persistent_ptr<NodePool> pool,
				uint64_t data, uint64_t tag);

		inline bool dequeue(size_t thread_index,
				pmem::obj::persistent_ptr<QueueType> queue,
				pmem::obj::persistent_ptr<NodePool> pool,
				uint64_t *data, uint64_t tag);

		inline void waitForExperiment();
	};
}

#include "QueueTestBase-inl.h"

#endif // DSS_QUEUE_TEST_BASE_H