#!/bin/bash

# This script runs the program with the producer-cosnumer application.
# It will create a CSV file called prod-cons-DSS-ADSS-UDSS.csv
# with the throughput of DSS, ADSS, and UDSS queue implementations.

MMFILE=/mnt/pmem_numa3/${USER}_dssq
CORES="3,7,11,15,19,23,27,31,35,39,43,47,51,55,59,63,67,71,75,79"
NUM_OF_EXPR=10

exectue() {
    THRU=`taskset -c $CORES ./DSSQueue \
            -mem_map_path=$MMFILE -queue_type=$queue_type\
            -test_type="producer-consumer" \
            -num_producers=$num_producers -num_consumers=$num_consumers\
            | grep Throughput | cut -d' ' -f3`

    echo "${num_producers}, ${num_consumers}, ${THRU}"
    avg=$(($avg + $THRU))
}

run_performance_test() {
    rm prod-cons-$queue_type.csv

    echo "num_producers,num_consumers,$queue_type" >> prod-cons-$queue_type.csv
    num_producers=1
    num_consumers=1
    avg=0

    for (( j=1; j<=$NUM_OF_EXPR; j++ ))
    do
        rm $MMFILE
        exectue
    done

    echo $num_producers,$num_consumers,$((avg / NUM_OF_EXPR)) >> prod-cons-$queue_type.csv
}

cd ..
mkdir -p build
cd build
cmake ..
make -j20

for queue_type in {"ADSS","UDSS","DSS"}; do
    echo "--------------------------"
    echo $queue_type

    run_performance_test
done

awk -F, '{print $3}' OFS=, prod-cons-UDSS.csv | paste -d, prod-cons-ADSS.csv - > prod-cons-ADSS-UDSS.csv
awk -F, '{print $3}' OFS=, prod-cons-DSS.csv | paste -d, prod-cons-ADSS-UDSS.csv - > prod-cons-DSS-ADSS-UDSS.csv