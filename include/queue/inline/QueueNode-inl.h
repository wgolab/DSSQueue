#ifndef DSS_QUEUE_NODE_INL_H
#define DSS_QUEUE_NODE_INL_H

#ifndef DSS_QUEUE_NODE_H
#error "QueueNode-inl.h" should be included only in "QueueNode.h" file.
#endif

#include "QueueNode.h"

#include <iostream>
#include <cstdint>
#include <cstddef>
#include <unordered_map>
#include <libpmemobj++/make_persistent.hpp>
#include <libpmemobj++/p.hpp>
#include <libpmemobj++/pool.hpp>
#include <libpmemobj++/transaction.hpp>

DSS::QueueNode::QueueNode() {
    // the last node in the queue should have its next_ pointer set to itself
    // NULL pointer means the node has not been allocated
    next_ = NULL;
    data_ = 0;
    // fields that are updated during a dequeue operation should be
    // on a separate cache line from next_ pointer
    epoch_deleted_ = 0;
    del_thread_index_ = -1;
}

void DSS::QueueNode::reset() {
    epoch_deleted_ = 0;
    del_thread_index_ = -1;
    next_ = NULL;
}

bool DSS::QueueNode::isFree() {
    return epoch_deleted_ == 0;
}

bool DSS::QueueNode::isUsed() {
    return epoch_deleted_ == NodeEpochManager::MAX_EPOCH;
}

bool DSS::QueueNode::isRemoved() {
    return epoch_deleted_ > 0 && epoch_deleted_ < NodeEpochManager::MAX_EPOCH;
}

void DSS::QueueNode::print() {
    std::cout << "Node " << this << " { next_ = <" << next_
    << ">, epoch_deleted_ = " << epoch_deleted_
    << ", del_thread_index_ = " << del_thread_index_
    << "}" << std::endl;
}

#endif // DSS_QUEUE_NODE_INL_H