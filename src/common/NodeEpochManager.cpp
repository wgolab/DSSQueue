#include "NodeEpochManager.h"
#include <cassert>
#include <iostream>
#include <stdio.h>

/*

  Credits: This source code file is a simplified version of
  https://github.com/microsoft/pmwcas/blob/master/src/common/epoch.cc
  written by Tianzheng Wang and Justin Levandoski (Copyright Microsoft, MIT License).

*/


using namespace DSS;

NodeEpochManager::NodeEpochManager()
		: current_epoch_(1)
		, safe_to_reclaim_epoch_(0)
		, epoch_table_(nullptr)
{
}

NodeEpochManager::NodeEpochManager(NodeEpoch val)
		: current_epoch_(val)
		, safe_to_reclaim_epoch_(val-1)
		, epoch_table_(nullptr)
{
}

void NodeEpochManager::initialize()
{
	// if(epoch_table_) return;

	NodeEpochTable* new_table = new NodeEpochTable();
	new_table->initialize();
	epoch_table_ = new_table;
	intialized = 0;
}

void NodeEpochManager::bumpCurrentEpoch() {
	NodeEpoch newEpoch = current_epoch_.fetch_add(1, std::memory_order_seq_cst);
	computeNewSafeToReclaimEpoch(newEpoch);
}

void NodeEpochManager::computeNewSafeToReclaimEpoch(NodeEpoch currentEpoch) {
	safe_to_reclaim_epoch_.store(
			epoch_table_->computeNewSafeToReclaimEpoch(currentEpoch),
			std::memory_order_release);
}

NodeEpoch NodeEpochManager::getSafeToReclaimEpoch() {
	NodeEpoch currentEpoch = getCurrentEpoch();
	computeNewSafeToReclaimEpoch(currentEpoch);
	return safe_to_reclaim_epoch_.load(std::memory_order_release);
}
