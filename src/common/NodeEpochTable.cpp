#include "NodeEpochTable.h"

#include <cassert>
#include <iostream>
#include <stdio.h>


/*

  Credits: This source code file is a simplified version of
  https://github.com/microsoft/pmwcas/blob/master/src/common/epoch.h
  written by Tianzheng Wang and Justin Levandoski (Copyright Microsoft, MIT License).

*/


using namespace DSS;

NodeEpochTable::NodeEpochTable()
: table_(nullptr),
size_(0)
{
}

void NodeEpochTable::initialize(uint64_t size) {
	NodeEntry* new_table= new NodeEntry[size];
	assert(!(reinterpret_cast<uintptr_t>(new_table)& (64 - 1)));

	table_ = new_table;
	this->table_ = table_;
	size_ = size;
}

void NodeEpochTable::protect(NodeEpoch current_epoch, size_t thread_id)
{
	NodeEntry* entry = nullptr;
	// store current epoch in thread entry

	getEntryForThread(&entry, thread_id);
	entry->protected_epoch.store(current_epoch, std::memory_order_release);
}

void NodeEpochTable::unprotect(size_t thread_id) {
	NodeEntry* entry = nullptr;
	getEntryForThread(&entry, thread_id);
	// store 0
	// keep track of which thread in which epoch
	entry->protected_epoch.store(0, std::memory_order_relaxed);
}

NodeEpoch NodeEpochTable::computeNewSafeToReclaimEpoch(NodeEpoch current_epoch) {

	NodeEpoch oldest_call = current_epoch;
	for(uint64_t i = 0; i < size_; ++i)
	{
		NodeEntry& entry = table_[i];
		NodeEpoch entryEpoch = entry.protected_epoch.load(std::memory_order_acquire);
		if(entryEpoch != 0 && entryEpoch < oldest_call) {
			oldest_call = entryEpoch;
		}
	}
	// The latest safe epoch is the one just before the earlier unsafe one.
	return oldest_call - 1;
}

void NodeEpochTable::getEntryForThread(NodeEntry** entry, size_t thread_id) {
	thread_local NodeEntry *tls = nullptr;
	if(tls) {
		*entry = tls;
		return;
	}


	NodeEntry* reserved = reserveEntryForThread(thread_id);
	tls = *entry = reserved;
}

uint32_t NodeEpochTable::NodeMurmur3(uint32_t h) {
	h ^= h >> 16;
	h *= 0x85ebca6b;
	h ^= h >> 13;
	h *= 0xc2b2ae35;
	h ^= h >> 16;
	return h;
}

NodeEntry* NodeEpochTable::reserveEntryForThread(size_t thread_id) {
	uint64_t current_thread_id = static_cast<uint64_t>(thread_id);
	uint64_t startIndex = NodeMurmur3(current_thread_id);
	return reserveEntry(startIndex, current_thread_id);
}

NodeEntry* NodeEpochTable::reserveEntry(uint64_t start_index,
										uint64_t thread_id)
{
	for(uint64_t i = 0; i < size_; ++i)
	{
		uint64_t indexToTest = (start_index + i) & (size_ - 1);
		NodeEntry& entry = table_[indexToTest];

		if(entry.thread_id == 0) {
			uint64_t expected = 0;
			bool success = entry.thread_id.compare_exchange_strong(
					expected, thread_id, std::memory_order_relaxed);
			if(success) {
				return &table_[indexToTest];
			}
		}
	}
	assert(false);
	return 0; // WG: this line should never be reached, added it to remove compiler warning
}
