#ifndef UDSS_QUEUE_INL_H
#define UDSS_QUEUE_INL_H

#ifndef UDSS_QUEUE_H
#error "UDSSQueue-inl.h" should be included only in "UDSSQueue.h" file.
#endif

#include "UDSSQueue.h"
#include <libpmem.h>

void DSS::UDSSQueue::initialize() {
    ADSSQueue::initialize();
}

void DSS::UDSSQueue::recoverable_enqueue(int thread_id, pmem::obj::
    persistent_ptr<DSS::QueueNode> node, uint64_t tag) {

  DSS::UDSSQueue::resolve(thread_id);
  ADSSQueue::enqueue_combined(thread_id, node, tag);
}

bool DSS::UDSSQueue::recoverable_dequeue(int thread_id,
    DSS::QueueNode **node, uint64_t tag) {
  
  DSS::UDSSQueue::resolve(thread_id);
  return ADSSQueue::dequeue_combined(thread_id, node, tag);
}

void DSS::UDSSQueue::enqueue(int thread_id, pmem::obj::
    persistent_ptr<DSS::QueueNode> node) {

  ADSSQueue::exec_enqueue(thread_id, node);
}

bool DSS::UDSSQueue::dequeue(int thread_id, DSS::QueueNode **node) {
  
  return ADSSQueue::exec_dequeue(thread_id, node);
}

bool DSS::UDSSQueue::dequeue_combined(int thread_id,
    DSS::QueueNode **node, uint64_t tag) {
  
  return DSS::UDSSQueue::recoverable_dequeue(thread_id, node, tag);
}

void DSS::UDSSQueue::enqueue_combined(int thread_id, pmem::obj::
    persistent_ptr<DSS::QueueNode> node, uint64_t tag) {

  DSS::UDSSQueue::recoverable_enqueue(thread_id, node, tag);
}

void DSS::UDSSQueue::recover() {
  ADSSQueue::recover();
}

std::string DSS::UDSSQueue::toString() {
  return "UDSS";
}

bool DSS::UDSSQueue::isDSS() {
  return false;
}

#endif // UDSS_QUEUE_INL_H