#ifndef QUEUE_BENCHMARK_H
#define QUEUE_BENCHMARK_H

#include <stdlib.h>
#include <unistd.h>
#include <cstdint>
#include <iostream>
#include <random>
#include <thread>
#include <cmath>
#include <climits>
#include <filesystem>

#include <libpmem.h>
#include <libpmemobj++/make_persistent.hpp>
#include <libpmemobj++/p.hpp>
#include <libpmemobj++/persistent_ptr.hpp>
#include <libpmemobj++/pool.hpp>
#include <libpmemobj++/transaction.hpp>
#include <gflags/gflags.h>

#include "NodeEpochManager.h"
#include "ADSSQueue.h"
#include "UDSSQueue.h"
#include "PerformanceTest.h"
#include "ResolutionTest.h"
#include "OverheadTest.h"
#include "ProducerConsumerTest.h"

// use gflags for variables can be modified using cmd
DECLARE_uint64(queue_size);
DECLARE_uint64(threads);
DECLARE_uint64(num_seconds);
DECLARE_uint64(num_producers);
DECLARE_uint64(num_consumers);
DECLARE_string(mem_map_path);
DECLARE_string(test_type);
DECLARE_string(queue_type);

#define POOL_SIZE 1024 * 1024 * 16
#define LAYOUT ""

#define PERFORMANCE_TEST "performance"
#define RESOLUTION_TEST "resolution"
#define OVERHEAD_TEST "overhead"
#define PRODUCER_CONSUMER_TEST "producer-consumer"

template <class QueueType, class TestType>
class QueueBenchmark {
public:
    QueueBenchmark() {}
    int initialize();
    void run();
    int teardown();
    void createQueue();
    
    pmem::obj::pool<DSS::Root<QueueType>> pool;
    pmem::obj::persistent_ptr<DSS::Root<QueueType>> root;
};

#endif // QUEUE_BENCHMARK_H
