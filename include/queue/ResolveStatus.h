#ifndef DSS_RESOLVE_STATUS_H
#define DSS_RESOLVE_STATUS_H

#include <cstdint>
#include <libpmemobj++/persistent_ptr.hpp>

namespace DSS {

	enum OperationEnum: uint64_t {
		NO_OP,
		DEQUEUE,
		ENQUEUE
	};

	enum StatusEnum: uint64_t {
		FAIL,
		EMPTY
	};

	struct ResolveStatus {
	  inline ResolveStatus(uint64_t status_, uint64_t operation_tag_);
		inline ResolveStatus();
		inline bool operator==(const ResolveStatus& other);

		pmem::obj::p<uint64_t> status;
		pmem::obj::p<uint64_t> operation_tag;
	};
}

#include "ResolveStatus-inl.h"

#endif // DSS_RESOLVE_STATUS_H