#!/bin/bash

# This script runs the program to test
# the correctness of resolution method.

MMFILE=/mnt/pmem_numa3/${USER}_dssq
CORES="3,7,11,15,19,23,27,31,35,39,43,47,51,55,59,63,67,71,75,79"

cd ..
mkdir -p build
cd build
cmake ..
make -j20

THREADS=20

rm $MMFILE
echo "DSS [Resolution]"
taskset -c $CORES ./DSSQueue -threads=$THREADS -mem_map_path=$MMFILE -test_type="resolution"
echo "--------------------------------------------------------------"

rm $MMFILE
echo "--------------------------------------------------------------"
echo "UDSS [Resolution]"
taskset -c $CORES ./DSSQueue -threads=$THREADS -mem_map_path=$MMFILE -queue_type="UDSS" -test_type="resolution"

echo "--------------------------------------------------------------"