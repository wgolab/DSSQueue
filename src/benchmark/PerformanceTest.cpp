#include "PerformanceTest.h"

using namespace DSS;

template class PerformanceTest<ADSSQueue>;
template class PerformanceTest<UDSSQueue>;
template class PerformanceTest<DSSQueue>;

template<class QueueType>
PerformanceTest<QueueType>::PerformanceTest()
: QueueTestBase<QueueType>(),
  total_success_(0),
  enqNum_(0),
  deqNum_(0)
{
  for (size_t i = 0; i < MAX_THREADS; i++) {
    this->threadIdOpNumMap_[i] = 0;
  }

  for (size_t i = 0; i < (MAX_THREADS<<DSS::PADDING_BITS); i++) {
    A[i] = 0;
    B[i] = 0;
  }
}

template<class QueueType>
void PerformanceTest<QueueType>::startWorkerThreads(
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<NodePool> pool,
		std::thread* threads[]) {

	// Start threads
	std::cout << "Running experiment for " << FLAGS_num_seconds << " seconds" << std::endl;

	// add barrier
	this->barrier = FLAGS_threads;
	this->finish_barrier = FLAGS_threads;
	this->finish_flag = 0;
	int timeStart = clock();
	
	for (unsigned int i = 0; i < FLAGS_threads; i++) {
		threads[i] = new std::thread(&PerformanceTest<QueueType>::main, this, i, queue, pool);
	}
}

template<class QueueType>
void PerformanceTest<QueueType>::run(
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<NodePool> pool) {

  std::cout << "*****************************************" << std::endl;

  std::cout << "Starting " << QueueType::toString() << " benchmark with " <<
      FLAGS_threads << " threads " << std::endl;

	this->setup(queue, pool);

	std::thread* threads[FLAGS_threads];
	this->startWorkerThreads(queue, pool, threads);
	this->waitForExperiment();

	this->reportExperimentResult(queue);
	this->teardown(threads);
}

template<class QueueType>
void PerformanceTest<QueueType>::enqueue(size_t thread_index,
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<NodePool> pool,
    uint64_t n_enq) {
    
  uint64_t tag = NO_UDSS_TAG;
  pmem::obj::persistent_ptr<QueueNode> node = 
      this->getNewNode(thread_index, queue, pool, n_enq);

  if (queue->isDSS()) {
    A[thread_index<<DSS::PADDING_BITS] = n_enq;
    pmem_persist(&A[thread_index<<DSS::PADDING_BITS], sizeof(A[thread_index<<DSS::PADDING_BITS]));
    queue->prep_enqueue(thread_index, node, tag);
    
    B[thread_index<<DSS::PADDING_BITS] = n_enq;
    pmem_persist(&B[thread_index<<DSS::PADDING_BITS], sizeof(B[thread_index<<DSS::PADDING_BITS]));
    queue->exec_enqueue(thread_index, node);

  } else {

    // equivalent to (i mod 2^(k - 1)) == 0)
    if (n_enq & (MAX_TAG_VALUE >> 1) == 0) {

      // equivalent to (i / 2^(k - 1)) == 0)
      A[thread_index<<DSS::PADDING_BITS] = (n_enq >> (NUM_OF_TAG_BITS - 1));
      pmem_persist(&A[thread_index<<DSS::PADDING_BITS], sizeof(A[thread_index<<DSS::PADDING_BITS]));
    }
      // equivalent to (i mod 2^k)
      tag = n_enq & MAX_TAG_VALUE;
      queue->enqueue_combined(thread_index, node, tag);
  }
}

template<class QueueType>// called for each thread
void PerformanceTest<QueueType>::main(size_t thread_index,
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<NodePool> pool) {
  this->barrier--;

  // Each worker thread waits for the others to reach the above line.
  while (this->barrier > 0);

  this->recoverNum_.fetch_add(1, std::memory_order_seq_cst);
  while(this->recoverNum_.load() < FLAGS_threads);

  //std::cout << "[INFO] set up finished successfully." << std::endl;

  // actual start
  uint64_t n_success = 0;
  uint64_t n_enq = 0;
  uint64_t n_deq = 0;
  
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distrib(0, 1);

  try
  {
    while (!this->finish_flag) {
      int r = distrib(gen);

      this->epochMgr.protect(thread_index);

      if (r == 0) {
        // enqueue then dequeue
        n_enq++;
        this->enqueue(thread_index, queue, pool, n_enq);
        uint64_t data;
        if (this->dequeue(thread_index, queue, pool, &data, NO_UDSS_TAG))
    n_deq++;
      } else {
        // dequeue then enqueue
        uint64_t data;
        if (this->dequeue(thread_index, queue, pool, &data, NO_UDSS_TAG))
    n_deq++;
        n_enq++;
        this->enqueue(thread_index, queue, pool, n_enq);
      }
      n_success += 2;

      /*
      // BEGIN - Nan Li's workload
      n_enq++;
      enqueue(thread_index, queue, pool, n_enq, NO_UDSS_TAG);
      n_enq++;
      enqueue(thread_index, queue, pool, n_enq, NO_UDSS_TAG);
      uint64_t data;
      if (dequeue(thread_index, queue, pool, &data, NO_UDSS_TAG))
        n_deq++;
      if (dequeue(thread_index, queue, pool, &data, NO_UDSS_TAG))
        n_deq++;
      n_success += 4;
      // END - Nan Li's workload
      */

      this->epochMgr.unprotect(thread_index);

      if (n_enq % (NUM_NODES_PER_PARTITION / 2) == 0)
        this->epochMgr.bumpCurrentEpoch();
    }

  }
  catch(const std::exception& e) {}
  
  this->threadIdOpNumMap_[thread_index] = n_success;
  this->total_success_.fetch_add(this->threadIdOpNumMap_[thread_index], std::memory_order_seq_cst);	
  this->enqNum_.fetch_add(n_enq, std::memory_order_seq_cst);
  this->deqNum_.fetch_add(n_deq, std::memory_order_seq_cst);

  this->finish_barrier--;
}

template<class QueueType>
void DSS::PerformanceTest<QueueType>::reportExperimentResult( 
		pmem::obj::persistent_ptr<QueueType> queue) {

	size_t s = queue->size();
	std::cout << "Final queue size is " << s << std::endl;

	std::cout << "Throughput (ops/s): " << total_success_/FLAGS_num_seconds << std::endl;
	
	if (total_success_ > 0)
		std::cout << "Time per operation (ns): " <<
				FLAGS_num_seconds * 1e9 * FLAGS_threads / total_success_ << std::endl;
	else
		std::cout << "Time per operation (ns): N/A" << std::endl;
}
