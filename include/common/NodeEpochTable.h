#ifndef DSS_NODE_EPOCH_TABLE_H
#define DSS_NODE_EPOCH_TABLE_H

#include <atomic>
#include <cstdint>
#include <list>
#include <mutex>

#include "NodeEntry.h"


/*

  Credits: This source code file is a simplified version of
  https://github.com/microsoft/pmwcas/blob/master/src/common/epoch.h
  written by Tianzheng Wang and Justin Levandoski (Copyright Microsoft, MIT License).

*/


namespace DSS {

	class NodeEpochTable {
	public:
		static const uint64_t kDefaultSize = 1024;

		NodeEpochTable();
		void initialize(uint64_t size = NodeEpochTable::kDefaultSize);
		void protect(NodeEpoch currentEpoch, size_t thread_id);
		void unprotect(size_t thread_id);

		NodeEpoch computeNewSafeToReclaimEpoch(NodeEpoch currentEpoch);

		void getEntryForThread(DSS::NodeEntry** entry, size_t thread_id);
		DSS::NodeEntry* reserveEntry(uint64_t startIndex, uint64_t threadId);
		DSS::NodeEntry* reserveEntryForThread(size_t thread_id);

		DSS::NodeEntry* table_;
		uint64_t size_;

		uint32_t NodeMurmur3(uint32_t h);
	};
}

#endif // DSS_NODE_EPOCH_TABLE_H
