#include <libpmem.h>
#include "NodePool.h"


using namespace DSS;

void NodePool::recover() {
  for (int i = 0; i < NUM_PARTITIONS; i++) {
    partitions[i]->recover();
  }
}
