#ifndef DSS_RESOLVE_STATUS_INL_H
#define DSS_RESOLVE_STATUS_INL_H

#ifndef DSS_RESOLVE_STATUS_H
#error "ResolveStatus-inl.h" should be included only in "ResolveStatus.h" file.
#endif

DSS::ResolveStatus::ResolveStatus(uint64_t status_,
    uint64_t operation_tag_) {
  this->status = status_;
  this->operation_tag = operation_tag_;
}

DSS::ResolveStatus::ResolveStatus() {
  this->status = StatusEnum::FAIL;
  this->operation_tag = 0;
}

bool DSS::ResolveStatus::operator==(const ResolveStatus& other) {
    return this->status == other.status
        && this->operation_tag == other.operation_tag;
}

#endif // DSS_RESOLVE_STATUS_INL_H