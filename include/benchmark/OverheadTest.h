#ifndef DSS_OVERHEAD_TEST_H
#define DSS_OVERHEAD_TEST_H

#include "QueueTestBase.h"

namespace DSS {
	template<class QueueType> class QueueTestBase;

	template<class QueueType>
    class OverheadTest : public QueueTestBase<QueueType> {
		private:
			long long enqueue_times_overall[MAX_THREADS << PADDING_BITS];
			long long enqueue_times_work[MAX_THREADS << PADDING_BITS];
			long long dequeue_times_overall[MAX_THREADS << PADDING_BITS];
			long long dequeue_times_work[MAX_THREADS << PADDING_BITS];
			long long num_operations[MAX_THREADS << PADDING_BITS];

			std::atomic<uint64_t> total_success_;
			std::atomic<uint64_t> enqNum_;
			std::atomic<uint64_t> deqNum_;
			
			int threadIdOpNumMap_[MAX_THREADS];

		public:
			OverheadTest();

			void startWorkerThreads(
					pmem::obj::persistent_ptr<QueueType> queue,
					pmem::obj::persistent_ptr<NodePool> pool,
					std::thread* threads[]);

			void reportExperimentResult(
					pmem::obj::persistent_ptr<QueueType> Queue);

			void run(
				pmem::obj::persistent_ptr<QueueType> queue,
				pmem::obj::persistent_ptr<NodePool> pool);

			void main(size_t thread_index,
				pmem::obj::persistent_ptr<QueueType> queue,
				pmem::obj::persistent_ptr<NodePool> pool);

			void enqueue(size_t thread_index,
				pmem::obj::persistent_ptr<QueueType> queue,
				pmem::obj::persistent_ptr<NodePool> pool,
				uint64_t data);

			bool dequeue(size_t thread_index,
				pmem::obj::persistent_ptr<QueueType> queue,
				pmem::obj::persistent_ptr<NodePool> pool,
				uint64_t *data);

    };
}

#endif // DSS_QUEUE_TEST_H