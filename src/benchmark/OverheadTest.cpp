#include <random>

#include "OverheadTest.h"
#include <chrono>
#include <stdio.h>

using namespace std::chrono;
using namespace DSS;

template class OverheadTest<ADSSQueue>;
template class OverheadTest<UDSSQueue>;
template class OverheadTest<DSSQueue>;

template<class QueueType>
DSS::OverheadTest<QueueType>::OverheadTest()
: total_success_(0),
enqNum_(0),
deqNum_(0)
{
  for (size_t i = 0; i < MAX_THREADS; i++) {
    this->threadIdOpNumMap_[i] = 0;
  }
  for (size_t i = 0; i < (MAX_THREADS << PADDING_BITS); i++) {
		enqueue_times_overall[i] = 0;
		enqueue_times_work[i] = 0;
		dequeue_times_overall[i] = 0;
		dequeue_times_work[i] = 0;
		num_operations[i] = 0;
	}
}

template<class QueueType>
void OverheadTest<QueueType>::startWorkerThreads(
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<NodePool> pool,
		std::thread* threads[]) {

	// Start threads
	std::cout << "Running experiment for " << FLAGS_num_seconds << " seconds" << std::endl;

	// add barrier
	this->barrier = FLAGS_threads;
	this->finish_barrier = FLAGS_threads;
	this->finish_flag = 0;
	int timeStart = clock();
	
	for (unsigned int i = 0; i < FLAGS_threads; i++) {
		threads[i] = new std::thread(&OverheadTest::main, this, i, queue, pool);
	}
}

template<class QueueType>
void OverheadTest<QueueType>::reportExperimentResult(
		pmem::obj::persistent_ptr<QueueType> queue) {

	size_t s = queue->size();
	std::cout << "Final queue size is " << s << std::endl;

	std::cout << "Throughput (ops/s): " << total_success_/FLAGS_num_seconds << std::endl;
	
	if (total_success_ > 0)
		std::cout << "Time per operation (ns): " <<
				FLAGS_num_seconds * 1e9 * FLAGS_threads / total_success_ << std::endl;
	else
		std::cout << "Time per operation (ns): N/A" << std::endl;
	
	long double total_time_overall = 0;
	long double total_time_work = 0;
	long double total_time_overhead = 0;
	long double total_enqueue_time_overall = 0;
	long double total_enqueue_time_work = 0;
	long double total_dequeue_time_overall = 0;
	long double total_dequeue_time_work = 0;
	long long int total_operations = 0;

	for (unsigned int i = 0; i < FLAGS_threads; i++) {
		total_enqueue_time_overall += enqueue_times_overall[i << PADDING_BITS];
		total_enqueue_time_work += enqueue_times_work[i << PADDING_BITS];
		total_dequeue_time_overall += dequeue_times_overall[i << PADDING_BITS];
		total_dequeue_time_work += dequeue_times_work[i << PADDING_BITS];
		total_operations += num_operations[i << PADDING_BITS];
	}

	total_time_overall = (total_enqueue_time_overall + total_dequeue_time_overall)/total_operations/1000;
	total_time_work = (total_enqueue_time_work + total_dequeue_time_work)/total_operations/1000;
	total_time_overhead = total_time_overall - total_time_work;
	total_enqueue_time_overall /= total_operations*1000;
	total_enqueue_time_work /= total_operations*1000;
	total_dequeue_time_overall /= total_operations*1000;
	total_dequeue_time_work /= total_operations*1000;
	
	printf("Overhead %lld,%LF,%LF,%LF,%LF,%LF\n",total_operations,total_enqueue_time_work,total_dequeue_time_work,total_time_work,total_time_overhead,total_time_overall);
}

template<class QueueType>
void OverheadTest<QueueType>::run(
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<NodePool> pool) {

	this->setup(queue, pool);

	std::thread* threads[FLAGS_threads];
	startWorkerThreads(queue, pool, threads);
	this->waitForExperiment();

	reportExperimentResult(queue);

	this->teardown(threads);
}

template<class QueueType>// called for each thread
void OverheadTest<QueueType>::main(size_t thread_index,
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<NodePool> pool) {

  this->barrier--;

  // Each worker thread waits for the others to reach the above line.
  while (this->barrier > 0);

  this->recoverNum_.fetch_add(1, std::memory_order_seq_cst);
  while(this->recoverNum_.load() < FLAGS_threads);

  //std::cout << "[INFO] set up finished successfully." << std::endl;

  // actual start
  uint64_t n_success = 0;
  uint64_t n_enq = 0;
  uint64_t n_deq = 0;
  
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distrib(0, 1);

  while (!this->finish_flag) {
    int r = distrib(gen);

    this->epochMgr.protect(thread_index);

    if (r == 0) {
      // enqueue then dequeue
      n_enq++;
      enqueue(thread_index, queue, pool, n_enq);
      uint64_t data;
      if (dequeue(thread_index, queue, pool, &data))
	n_deq++;
    } else {
      // dequeue then enqueue
      uint64_t data;
      if (dequeue(thread_index, queue, pool, &data))
	n_deq++;
      n_enq++;
      enqueue(thread_index, queue, pool, n_enq);
    }
    n_success += 2;

    this->epochMgr.unprotect(thread_index);

    if (n_enq % (NUM_NODES_PER_PARTITION / 2) == 0)
      this->epochMgr.bumpCurrentEpoch();
  }

  threadIdOpNumMap_[thread_index] = n_success;
  this->total_success_.fetch_add(threadIdOpNumMap_[thread_index], std::memory_order_seq_cst);	
  this->enqNum_.fetch_add(n_enq, std::memory_order_seq_cst);
  this->deqNum_.fetch_add(n_deq, std::memory_order_seq_cst);

  this->finish_barrier--;
}

template<class QueueType>
void DSS::OverheadTest<QueueType>::enqueue(size_t thread_index,
			pmem::obj::persistent_ptr<QueueType> queue,
			pmem::obj::persistent_ptr<NodePool> pool,
			uint64_t data) {
	auto code_start = high_resolution_clock::now();

	pmem::obj::persistent_ptr<QueueNode> newNode = 
			this->getNewNode(thread_index, queue, pool, data);

	auto work_start = high_resolution_clock::now();

	queue->enqueue_combined(thread_index, newNode, NO_UDSS_TAG);
	auto code_stop = high_resolution_clock::now();

	// store time spent overall and time spent doing useful work during the dequeue operation 
	enqueue_times_overall[thread_index << PADDING_BITS] += (int)duration_cast<nanoseconds>(code_stop - code_start).count();
	enqueue_times_work[thread_index << PADDING_BITS] += (int)duration_cast<nanoseconds>(code_stop - work_start).count();
	num_operations[thread_index << PADDING_BITS] += 1;
}

template<class QueueType>
bool DSS::OverheadTest<QueueType>::dequeue(size_t thread_index,
			pmem::obj::persistent_ptr<QueueType> queue,
			pmem::obj::persistent_ptr<NodePool> pool,
			uint64_t *data) {
	auto code_start = high_resolution_clock::now();
  QueueNode *deqNode;

	bool ret = queue->dequeue_combined(thread_index, &deqNode, NO_UDSS_TAG);

  if (ret) {
    // successfully dequeued a node
    *data = deqNode->next_->data_;
	auto overhead_start = high_resolution_clock::now();
    pool->free(thread_index, &this->epochMgr, deqNode);

	auto code_stop = high_resolution_clock::now();
	// store time spent overall and time spent doing useful work during the enqueue operation 
	dequeue_times_overall[thread_index << PADDING_BITS] += (int)duration_cast<nanoseconds>(code_stop - code_start).count();
	dequeue_times_work[thread_index << PADDING_BITS] += (int)duration_cast<nanoseconds>(overhead_start - code_start).count();
	num_operations[thread_index << PADDING_BITS] += 1;

  } else {
    // queue was empty
    *data = 0;
  }
  return ret;
}