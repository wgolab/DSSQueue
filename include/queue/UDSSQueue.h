#ifndef UDSS_QUEUE_H
#define UDSS_QUEUE_H

#include "ADSSQueue.h"
#include "Root.h"

namespace DSS {
	enum ResolveStatusPositions: int {
		STATUS = 1,
		OPERATION_TAG
	};
	
	class UDSSQueue : public ADSSQueue {
	public:

	  inline void recoverable_enqueue(int thread_id, pmem::obj::
	  		persistent_ptr<QueueNode> node, uint64_t tag);
		inline bool recoverable_dequeue(int thread_id, QueueNode **node, uint64_t tag);
		
		// The following two invoke the exec_op of DSSQueue
		inline void enqueue(int thread_id, pmem::obj::persistent_ptr<QueueNode> node);
		inline bool dequeue(int thread_id, QueueNode **node);

		// The following two methods are added to keep the same interface
		// with DSSQueue to be called from the benchmark
		inline void enqueue_combined(int thread_id, pmem::obj::
	  		persistent_ptr<QueueNode> node, uint64_t tag);
		inline bool dequeue_combined(int thread_id, QueueNode **node, uint64_t tag);

		ResolveStatus resolve(int thread_id);
		inline void initialize();
		inline void recover();

		inline static std::string toString();
		inline bool isDSS();
	};
}

#include "UDSSQueue-inl.h"

#endif // UDSS_QUEUE_H