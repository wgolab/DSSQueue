#ifndef DSS_ATOMICS_H
#define DSS_ATOMICS_H

#include <semaphore.h>
#include <atomic>

// BORROWED from PMWCAS
namespace DSS {
	template <typename T> T CompareExchange64(T* destination, T old_value, T new_value) {
		static_assert(sizeof(T) == 8,
					  "CompareExchange64 only works on 64 bit values");
		::__atomic_compare_exchange_n(destination, &old_value, new_value, false,
									  __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST);
		return old_value;
	}
}

#endif //DSS_ATOMICS_H