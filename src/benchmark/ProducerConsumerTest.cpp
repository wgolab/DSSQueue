#include "ProducerConsumerTest.h"

using namespace DSS;

template class ProducerConsumerTest<ADSSQueue>;
template class ProducerConsumerTest<UDSSQueue>;
template class ProducerConsumerTest<DSSQueue>;

template<class QueueType>
ProducerConsumerTest<QueueType>::ProducerConsumerTest()
: PerformanceTest<QueueType>()
{
}

template<class QueueType>
void ProducerConsumerTest<QueueType>::startWorkerThreads(
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<NodePool> pool,
		std::thread* threads[]) {

	// Start threads
	std::cout << "Running experiment for " << FLAGS_num_seconds << " seconds" << std::endl;

	// add barrier
	this->barrier = FLAGS_threads;
	this->finish_barrier = FLAGS_threads;
	this->finish_flag = 0;
	int timeStart = clock();
	
	for (unsigned int i = 0; i < FLAGS_num_producers; i++) {
		threads[i] = new std::thread(&ProducerConsumerTest<QueueType>::produce,
        this, i, queue, pool);
	}

  for (unsigned int i = FLAGS_num_producers; i < FLAGS_threads; i++) {
		threads[i] = new std::thread(&ProducerConsumerTest<QueueType>::consume,
        this, i, queue, pool);
	}
}

template<class QueueType>
void ProducerConsumerTest<QueueType>::run(
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<NodePool> pool) {

  std::cout << "*****************************************" << std::endl;

  std::cout << "Starting " << QueueType::toString() << " benchmark with " <<
      FLAGS_num_producers << " producers and " << FLAGS_num_consumers <<
      " consumers." << std::endl;

  FLAGS_threads = FLAGS_num_producers + FLAGS_num_consumers;
	this->setup(queue, pool);

	std::thread* threads[FLAGS_threads];
	this->startWorkerThreads(queue, pool, threads);
	this->waitForExperiment();

	this->reportExperimentResult(queue);
	this->teardown(threads);
}

template<class QueueType>
uint64_t ProducerConsumerTest<QueueType>::getStartingPoint(
    size_t thread_index,
		pmem::obj::persistent_ptr<QueueType> queue) {
    
    ResolveStatus resolveStatus = queue->resolve(thread_index);
    uint64_t tag = resolveStatus.operation_tag >> NUM_OF_DSS_RELATED_BITS;
    uint64_t operation = resolveStatus.operation_tag & OP_TAG_MASK;

    uint64_t lsba = this->A[thread_index<<DSS::PADDING_BITS] & 1;
    uint64_t msbt = tag & (1 << (NUM_OF_TAG_BITS - 1));
    uint64_t ltag = 0;

    uint64_t start = 1;

    if (operation == OperationEnum::NO_OP) {
      return start;
    } else if (lsba == msbt) {
      // equivalent to (tag mod 2^(k - 1))
      ltag = tag & (MAX_TAG_VALUE >> 1);
      if (resolveStatus.status == StatusEnum::FAIL) {
        start = (this->A[thread_index<<DSS::PADDING_BITS] << (NUM_OF_TAG_BITS - 1)) + ltag;
      } else {
        start = (this->A[thread_index<<DSS::PADDING_BITS] << (NUM_OF_TAG_BITS - 1)) + ltag + 1;
      }
    } else {
      start = (this->A[thread_index<<DSS::PADDING_BITS] << (NUM_OF_TAG_BITS - 1));
    }

    return start;
}

template<class QueueType>// called for each thread
void ProducerConsumerTest<QueueType>::produce(size_t thread_index,
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<NodePool> pool) {

  this->barrier--;

  // Each worker thread waits for the others to reach the above line.
  while (this->barrier > 0);

  this->recoverNum_.fetch_add(1, std::memory_order_seq_cst);
  while(this->recoverNum_.load() < FLAGS_threads);

  // actual start
  uint64_t n_success = 0;
  uint64_t tag = 0;
  uint64_t n_enq = getStartingPoint(thread_index, queue);
  
  try {
    while (!this->finish_flag) {

      this->epochMgr.protect(thread_index);
      n_enq++;
      this->enqueue(thread_index, queue, pool, n_enq);

      n_success += 1;

      this->epochMgr.unprotect(thread_index);
      this->epochMgr.bumpCurrentEpoch();
    }
  }
  catch(const std::exception& e) {}
  
  this->threadIdOpNumMap_[thread_index] = n_success;
  this->total_success_.fetch_add(this->threadIdOpNumMap_[thread_index], std::memory_order_seq_cst);	
  this->enqNum_.fetch_add(n_enq, std::memory_order_seq_cst);

  this->finish_barrier--;
}

template<class QueueType>// called for each thread
void ProducerConsumerTest<QueueType>::consume(size_t thread_index,
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<NodePool> pool) {

  this->barrier--;

  // Each worker thread waits for the others to reach the above line.
  while (this->barrier > 0);

  this->recoverNum_.fetch_add(1, std::memory_order_seq_cst);
  while(this->recoverNum_.load() < FLAGS_threads);

  // actual start
  uint64_t n_success = 0;
  uint64_t n_deq = 0;
  
  while (!this->finish_flag) {
    this->epochMgr.protect(thread_index);

    uint64_t data;
    if (this->dequeue(thread_index, queue, pool, &data, NO_UDSS_TAG))
      n_deq++;

    n_success += 1;

    this->epochMgr.unprotect(thread_index);
    this->epochMgr.bumpCurrentEpoch();
  }

  this->threadIdOpNumMap_[thread_index] = n_success;
  this->total_success_.fetch_add(this->threadIdOpNumMap_[thread_index], std::memory_order_seq_cst);	
  this->deqNum_.fetch_add(n_deq, std::memory_order_seq_cst);

  this->finish_barrier--;
}