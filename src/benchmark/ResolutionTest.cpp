#include "ResolutionTest.h"
#include "QueueBenchmark.h"

#include <random>
#include <bitset>

using namespace DSS;

template class ResolutionTest<ADSSQueue>;
template class ResolutionTest<UDSSQueue>;
template class ResolutionTest<DSSQueue>;

template<class QueueType>
void ResolutionTest<QueueType>::initializeUnitTests() {

	unit_tests.clear();

	unit_tests.push_back(&ResolutionTest<QueueType>::
			ItemEnqueuedCompEnqTagExpected);
	unit_tests.push_back(&ResolutionTest<QueueType>::
			ItemEnqueuedThenDequeuedCompDeqTagExpected);
	unit_tests.push_back(&ResolutionTest<QueueType>::
			prepEnqueueIsInvokedPrepEnqTagExpected);
	unit_tests.push_back(&ResolutionTest<QueueType>::
			ItemEnqueuedThenPrepDequeueIsInvokedPrepDeqTagExpected);

	num_of_failed_tests = 0;
	failed_tests.clear();
	for (int i = 0; i < MAX_THREADS; i++) {
		std::vector<std::string> temp;
		failed_tests.push_back(temp);
	}
}

template<class QueueType>
void ResolutionTest<QueueType>::startWorkerThreads(
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<DSS::NodePool> pool,
		std::thread* threads[], UnitTest unit_test) {

	// add barrier
	this->barrier = FLAGS_threads;
	this->finish_barrier = FLAGS_threads;
	this->finish_flag = 0;
	
	for (unsigned int i = 0; i < FLAGS_threads; i++) {
		threads[i] = new std::thread(unit_test, this, i, queue, pool);
	}
}

template<class QueueType>
void ResolutionTest<QueueType>::run_tests(
		const std::vector<UnitTest>& tests,
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<DSS::NodePool> pool) {

	for (UnitTest unit_test : tests) {
		
		this->epochMgr.initialize();
		this->setup(queue, pool);

		std::thread* threads[FLAGS_threads];
		this->startWorkerThreads(queue, pool, threads, unit_test);
		this->waitForExperiment();

		this->teardown(threads);
	}
}

template<class QueueType>
void ResolutionTest<QueueType>::run(
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<DSS::NodePool> pool) {

	FLAGS_num_seconds = 0;
	initializeUnitTests();
	
	// Single-threaded tests
	sprintf(result[0], "%s\n############ Single-threaded Tests ############\n%s",
			ColorCodes[BLUE], ColorCodes[RESET]);
	printf("%s", result[0]);

	FLAGS_queue_size = 0;
	FLAGS_threads = 1;
	run_tests(unit_tests, queue, pool);

	// Multi-threaded tests
	sprintf(result[0], "\n%s############ Multi-threaded Tests ############\n%s",
			ColorCodes[BLUE], ColorCodes[RESET]);
	printf("%s", result[0]);

	FLAGS_threads = 20;
	run_tests(unit_tests, queue, pool);

	if (num_of_failed_tests > 0) {
		printTestReport();
	} else {
		sprintf(result[0], "\n%sAll the resolution tests were passed successfully!\n%s",
				ColorCodes[BLUE], ColorCodes[RESET]);
		printf("%s", result[0]);
	}
}

template<class QueueType>
void ResolutionTest<QueueType>::setupUnitTest(size_t thread_index) {
  this->barrier--;
  while (this->barrier > 0);

  this->recoverNum_.fetch_add(1, std::memory_order_seq_cst);
  while(this->recoverNum_.load() < FLAGS_threads);
	this->epochMgr.protect(thread_index);
}

template<class QueueType>
void ResolutionTest<QueueType>::teardownUnitTest(size_t thread_index) {
	this->epochMgr.unprotect(thread_index);
  this->finish_barrier--;
}

template<class QueueType>
void ResolutionTest<QueueType>::printSuccess(
		size_t thread_index, std::string functionName) {

	sprintf(result[thread_index], "%s[%s for thread %ld]: PASSED!\n%s",
			ColorCodes[GREEN], functionName.c_str(), thread_index, ColorCodes[RESET]);

	printf ("%s", result[thread_index]);
}

template<class QueueType>
void ResolutionTest<QueueType>::printFailure(
		size_t thread_index, std::string functionName) {
			
	std::string test_name = "[" + functionName + " for thread " +
			std::to_string(thread_index) + "]";

	sprintf(result[thread_index], "%s%s: FAILED!\n%s",
			ColorCodes[RED], test_name.c_str(), ColorCodes[RESET]);

	printf ("%s", result[thread_index]);
	failed_tests[thread_index].push_back(test_name);
	num_of_failed_tests++;
}

template<class QueueType>
void ResolutionTest<QueueType>::printTestReport() {

	sprintf(result[0], "\n%s%d tests FAILED!\n%s",
				ColorCodes[RED], num_of_failed_tests, ColorCodes[RESET]);
	printf("%s", result[0]);

	for (int i = 0; i < failed_tests.size(); i++) {
		for (std::string failed_test : failed_tests[i]) {
			sprintf(result[0], "\n%s%s%s",
					ColorCodes[RED], failed_test.c_str(), ColorCodes[RESET]);
			printf("%s", result[0]);
		}
	}
	printf("\n");
}

// ############## Unit tests ##############

template<class QueueType>
void ResolutionTest<QueueType>::ItemEnqueuedCompEnqTagExpected(
		size_t thread_index,
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<DSS::NodePool> pool) {

	setupUnitTest(thread_index);

	uint64_t n_enq = 13 + thread_index;
	uint64_t tag = 18;
	this->enqueue(thread_index, queue, pool, n_enq, tag);
	ResolveStatus resolution = queue->resolve(thread_index);

	ResolveStatus expected = ResolveStatus(n_enq,
			DSS::OperationEnum::ENQUEUE | (tag << NUM_OF_DSS_RELATED_BITS));

	if (resolution == expected) {
		printSuccess(thread_index, __func__);
	} else {
		printFailure(thread_index, __func__);
	}
	teardownUnitTest(thread_index);
}

template<class QueueType>
void ResolutionTest<QueueType>::ItemEnqueuedThenDequeuedCompDeqTagExpected(
		size_t thread_index,
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<DSS::NodePool> pool) {

	setupUnitTest(thread_index);

	uint64_t n_enq = 13 + thread_index;
	uint64_t tag = 18;
	this->enqueue(thread_index, queue, pool, n_enq, tag);

	uint64_t data;
	this->dequeue(thread_index, queue, pool, &data, tag);
	DSS::ResolveStatus resolution = queue->resolve(thread_index);

	ResolveStatus expected = ResolveStatus(data,
			DSS::OperationEnum::DEQUEUE | (tag << NUM_OF_DSS_RELATED_BITS));

	if (resolution == expected) {
		printSuccess(thread_index, __func__);
	} else {
		printFailure(thread_index, __func__);
	}

	teardownUnitTest(thread_index);
}

template<class QueueType>
void ResolutionTest<QueueType>::prepEnqueueIsInvokedPrepEnqTagExpected(
		size_t thread_index,
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<DSS::NodePool> pool) {

	setupUnitTest(thread_index);

	uint64_t n_enq = 17 + thread_index;
	uint64_t tag = 28;

	// To ensure that we know the last successful operations
	this->enqueue(thread_index, queue, pool, n_enq, tag);

	n_enq++;
	tag++;

	pmem::obj::persistent_ptr<QueueNode> newNode =
			this->getNewNode(thread_index, queue, pool, n_enq);
  	
	queue->resolve(thread_index);
	queue->prep_enqueue(thread_index, newNode, tag);

	DSS::ResolveStatus resolution = queue->resolve(thread_index);
	ResolveStatus expected;

	if (queue->toString() == "UDSS") {
		expected = ResolveStatus(n_enq - 1,
				DSS::OperationEnum::ENQUEUE | ((tag - 1) << NUM_OF_DSS_RELATED_BITS));
	} else {
		expected = ResolveStatus(DSS::StatusEnum::FAIL,
				DSS::OperationEnum::ENQUEUE | (tag << NUM_OF_DSS_RELATED_BITS));
	}

	if (resolution == expected) {
		printSuccess(thread_index, __func__);
	} else {
		printFailure(thread_index, __func__);
	}

	teardownUnitTest(thread_index);
}

template<class QueueType>
void ResolutionTest<QueueType>::
		ItemEnqueuedThenPrepDequeueIsInvokedPrepDeqTagExpected(size_t thread_index,
		pmem::obj::persistent_ptr<QueueType> queue,
		pmem::obj::persistent_ptr<DSS::NodePool> pool) {

	setupUnitTest(thread_index);

	uint64_t n_enq = 13 + thread_index;
	uint64_t tag = 18;
	this->enqueue(thread_index, queue, pool, n_enq, tag);

	uint64_t data;
	QueueNode *deqNode;
	
	queue->resolve(thread_index);
	queue->prep_dequeue(thread_index, &deqNode, tag);

	DSS::ResolveStatus resolution = queue->resolve(thread_index);

	ResolveStatus expected;
	
	if (queue->toString() == "UDSS") {
		expected = ResolveStatus(n_enq,
				DSS::OperationEnum::ENQUEUE | (tag << NUM_OF_DSS_RELATED_BITS));
	} else {
		expected = ResolveStatus(DSS::StatusEnum::FAIL,
				DSS::OperationEnum::DEQUEUE | (tag << NUM_OF_DSS_RELATED_BITS));
	}

	if (resolution == expected) {
		printSuccess(thread_index, __func__);
	} else {
		printFailure(thread_index, __func__);
	}

	teardownUnitTest(thread_index);
}